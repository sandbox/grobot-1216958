
String.prototype.capitalize = function(){
  return this.replace(/\w+/g, function(a){
    return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase();
  });
};

String.prototype.capitalizeFirst = function(){
  return this.replace(/^./g, function(a){
    return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase();
  });
};

function file_import_form_change() {
  $('table').append('<tr><td></td><td></td><td id="c0"></td><td id="c1" style="vertical-align:top;width:40%;"></td><td id="c2" style="vertical-align:top;width:40%;"></td></tr>');

  $('#c1').append($('#replace_titles'));
  $('#c1').append($('#edit_titles'));
  $('#c2').append($('#target_node'));
  $('#c1 input').css('width', '100%');
  $('#file-import-form table input').css('width', '95%');

  $('#node_select_button').click(function(){
    $(".form-checkbox").each(function (i) {
      if ($(this).attr('checked')) {
        $('#edit-nid-'+$(this).attr('value')).attr('value',$('#node_select_combo').attr('value'));
      }
    });
    return false;
  });

  $('#clear_titles').click(function(){
    $(".form-checkbox").each(function (i) {
      if ($(this).attr('checked')) {
          $('#edit-title-'+$(this).attr('value')).attr('value','');
      }
    });
    return false;
  });

  $('#cfl_titles').click(function(){
    $(".form-checkbox").each(function (i) {
      if ($(this).attr('checked')) {
        a = '#edit-title-'+$(this).attr('value');
        $(a).attr('value',$(a).attr('value').capitalizeFirst());
      }
    });
    return false;
  });

  $('#cfla_titles').click(function(){
    $(".form-checkbox").each(function (i) {
      if ($(this).attr('checked')) {
        a = '#edit-title-'+$(this).attr('value');
        $(a).attr('value',$(a).attr('value').capitalize());
      }
    });
    return false;
  });
  
  stage = 0;
  $('#replace_titles_button').after('<input type="submit" id="replace_titles_button_cancel" value="' + Drupal.t('Cancel') + '" style="display:none;">');
  $('#replace_titles_button').click(function() {
    $(".form-checkbox").each(function (i) {
      edit = $('#edit-title-'+$(this).attr('value'));
      if (stage == 0) {
        if ($(this).attr('checked')) {
          value = edit.attr('value');
          from = $('#edit-replace-from').attr('value');
          to = $('#edit-replace-to').attr('value');
          var re = new RegExp(from, "g");
          value = value.replace(re,to);

          if (!edit.next('.description').length) {
            edit.after('<div class="description"></div>');
          }
          edit.next('.description').html(Drupal.t('Preview') + ': <span class="value">' + value + '</span>');
        }
      }
      else {
        edit.attr('value', edit.next('.description').children('.value').text());
        edit.next('.description').text('');
      }
    });

    if (stage == 0) {
      $('#replace_titles_button').attr('value', Drupal.t('Replace'));
      $('#replace_titles_button_cancel').show();
      stage = 1;
    }
    else if (stage == 1) {
      $('#replace_titles_button_cancel').trigger('click');
    }
    return false;
  });
  
  $('#replace_titles_button_cancel').click(function() {
    $(".form-checkbox").each(function (i) {
      if ($(this).attr('checked')) {
        edit.next('.description').remove();
      }
    });
    $('#replace_titles_button').attr('value', Drupal.t('Preview'));
    $('#replace_titles_button_cancel').hide();
    stage = 0;
    return false;
  });
}